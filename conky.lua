require 'string'
require 'lfs'

local function starts_with(str, start)
   return string.sub(str, 1, string.len(start)) == start
end

function conky_network_type ()
	local gw = conky_parse ("${gw_iface}")
	-- local gw = "wlo"
	if starts_with(gw, "en") then
		return " " .. gw
	elseif starts_with(gw, "wl") then
		local strength = tonumber(conky_parse("${wireless_link_qual_perc " .. gw .. " }"))
		return " ${wireless_essid " .. gw .. "} ".. strength .. "%"
	end
	return " ⚠ " .. gw
end

function conky_vpn ()
	str = ""
	for file in lfs.dir[[/sys/class/net/]] do
		if string.match(file, "mullvad") or file == "cw" then
			str = str .. '{"color":"#859900", "full_text": "  ' .. file .. ' "},'
		end
	end

	return str
end

function conky_battery_icon ()
	local percent = tonumber(conky_parse("$battery_percent"))
	if percent < 15 then
		return ""
	elseif percent < 35 then
		return ""
	elseif percent < 60 then
		return ""
	elseif percent < 85 then
		return ""
	else
		return ""
	end
end

function conky_audio_icon ()
	local port_desc = conky_parse("$pa_sink_active_port_description")
	if port_desc == "Headset" then
		return " "
	elseif port_desc == "Headphones" or port_desc == "Analog Output" then
		return ""
	elseif port_desc == "Speaker" then
		return ""
	end
	return conky_parse("$pa_sink_active_port_description")
end
